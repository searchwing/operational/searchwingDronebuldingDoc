# Table of contents

* [SearchWing Manual 0.2](README.md)
* [Template for instructions](template-for-instructions.md)

## Building the Searchwing-Drone <a href="#preparing-components" id="preparing-components"></a>

* [Software Setup](preparing-components/software-setup/README.md)
  * [Setup Pixracer](preparing-components/software-setup/setup-pixracer.md)
  * [Setup payload Pi Computers](preparing-components/software-setup/setup-companion-computer.md)
  * [Update RC firmware (OpenTX) and telemetry script (yaapu)](preparing-components/software-setup/update-rc-firmware-opentx-and-telemetry-script-yaapu.md)
  * [Create model on RC](preparing-components/software-setup/create-model-on-rc.md)
  * [Receiver Software](preparing-components/software-setup/receiver-software.md)
  * [Setup ESP for Pixracer](preparing-components/software-setup/setup-esp-for-pixracer.md)
  * [Setup RFD868+ radio](preparing-components/software-setup/setup-rfd868+-radio.md)
  * [Setup Groundcontrol Laptop](preparing-components/software-setup/setup-groundcontrol-laptop.md)
  * [Setup QGroundControl](preparing-components/software-setup/setup-qgroundcontrol.md)
* [Generic componets](preparing-components/generic-componets/README.md)
  * [Motor assembly and motor mount preparation](preparing-components/generic-componets/motor-assembly-and-motor-mount-preparation.md)
  * [Producing special cables](preparing-components/generic-componets/producing-special-cables.md)
  * [Waterproofing the motor](preparing-components/generic-componets/waterproofing-the-motor.md)
  * [Printing the box](preparing-components/generic-componets/printing-and-preparing-the-box.md)
  * [Finalizing the Box](preparing-components/generic-componets/finalizing-the-box.md)
  * [New cables for the servos](preparing-components/generic-componets/untitled-1.md)
  * [M8-Servo-Plugs](preparing-components/generic-componets/m5-servo-plugs.md)
  * [GPS Tower](preparing-components/generic-componets/gps-tower.md)
  * [Preparing the fuselage](preparing-components/generic-componets/preparing-the-fuselage.md)
  * [Batteries](preparing-components/generic-componets/batteries.md)
* [Combining Prepared Components](preparing-components/combining-components/README.md)
  * [Power cables](preparing-components/combining-components/power-cables.md)
  * [Test components before box assembly](preparing-components/combining-components/test-components-before-box-assembly.md)
  * [Add components to the box](preparing-components/combining-components/add-components-to-the-box.md)
  * [Preparing the water tight battery bag](preparing-components/combining-components/preparing-the-water-tight-battery-bag.md)
  * [Building V-Tails and Wings](preparing-components/combining-components/building-v-tails-and-wings.md)
  * [Equipping the box with all components](preparing-components/combining-components/equipping-the-box-with-all-components.md)
* [Final Assembly & Testing](preparing-components/final-assembly-and-testing/README.md)
  * [Setup and testing (open box)](preparing-components/final-assembly-and-testing/setup-and-testing-open-box.md)
  * [Closing the box](preparing-components/final-assembly-and-testing/closing-the-box.md)
  * ["Marriage" - combining box & fuselage](preparing-components/final-assembly-and-testing/marriage-combining-box-and-fuselage.md)
  * [Final testing and control](preparing-components/final-assembly-and-testing/final-testing.md)

## Testing & Development

* [How to create watertight 3D-prints](testing-and-development/how-to-create-watertight-3d-prints/README.md)
  * [The right filament - and the richt settings.](testing-and-development/how-to-create-watertight-3d-prints/the-right-filament.md)
  * [Detect thin walls setting](testing-and-development/how-to-create-watertight-3d-prints/detect-thin-walls-setting.md)
* [Watertight cable connections](testing-and-development/watertight-cable-connections.md)
* [The right glue for the right parts](testing-and-development/the-right-glue-for-the-right-parts.md)

## Parts

* [List of Parts and where to buy](parts/list-of-parts-and-where-to-buy.md)

## Using the drone

* [Pilot training book](using-the-drone/pilot-training-book.md)
* [Transport](using-the-drone/transport.md)
* [Packing list](using-the-drone/packing-list.md)
* [Pre-Flight](using-the-drone/untitled.md)
* [Flying](using-the-drone/flying.md)
* [After-Flight](using-the-drone/after-flight.md)

## Maintenance

* [Hardware](maintenance/hardware.md)
* [Software](maintenance/software.md)
