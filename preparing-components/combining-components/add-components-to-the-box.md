# Add components to the box

## Requirements

### Parts

* prepared Raspberry Pi compute module 4
* carrierboard for compute module 4
* 2 x camera cable
* 2 x Rasberry Pi camera
* prepared power cable
* prepared telemetry cable
* prepared DHT22 sensor/cable
* prepared power module incl. ESC and charging port
* prepared PixRacer
* prepared RFD868+ telemetry
* prepared RC receiver
* isolated shielding tape
* double-sided tape
* 8 x short screws (1.7 mm x 4 mm) for cameras
* 2 + 4 + 3 + 4 x long screws (2.2 mm x 6.5 mm)
* Weipu battery connector
* 4 x prepared M8 connectors
* Kabeldurchführung with Mehrfachdichteinsatz
* solder wire
* shrinking tube

### Tools

* hot glue gun
* cross-head screw driver
* soldering iron
* heat gun



## Assembly

If M-Plugs are not mounted in the box, do so.

## PI CM4

1. Connect DHT22 to CM4 before screwing it in (easier):&#x20;
   1. Red/Purple - VCC (3.3V or 5V) --> Pin 4 or Pin 1 (e.g. 1st pin in inner row)
   2. Yellow/Blue - Signal -->Pin 16 (8th pin in outer row)
   3. Black/Green - GND --> Pin 20 or 9 (e.g. 5th pin in inner row)
2. Shield both camera cables with isolated shielding tape. Glue the cables to the CM4 and the cameras. Make sure the heat sink is attached to the CPU.
3. Attach Power and Telemetry cables to the CM4.

![](../../.gitbook/assets/WIN\_20220530\_12\_50\_21\_Pro.jpg)

## Glue connections

If [test-components-before-box-assembly.md](test-components-before-box-assembly.md "mention") is done, glue all GPIO connectors with a hot glue gun to the Raspberry PI. Glue the shielded camera cables to the camera connectors as well as the Raspberry PI. Glue the SD cad of the Pixracer in-place.



4\. Screw the module to bottom with USB ports to tail/empennage.

5\. Screw cameras with short(!) screws to mounts: Camera0 to right, Camera1 to left (both connectors pointing to front).

6\. Glue DHT22 with double-sided tape below power connector.

![](../../.gitbook/assets/WIN\_20220530\_13\_17\_37\_Pro.jpg)

7\. Make sure the charging connector is soldered to the Power-Module.

8\. Put the battery power cable through the power connector hole and solder the Weipu connector to the cables. When Vinc is soldering, remind him to put both shrinking tube AND nut when connecting the power cable.

![](../../.gitbook/assets/WIN\_20220530\_14\_08\_12\_Pro.jpg)

9\. Tighten the nut on the power connector.&#x20;

10\. Make sure all cables (JST-Connectors) are attached to the Power-Module.&#x20;

11\. Screw the Power-Module with two screws (2.2 mm x 6.5 mm) into the Box.  (there are two additional screw holes for an additional version of the Power-Module.)

![](<../../.gitbook/assets/image (31).png>)

12\. Put the Nut from the ESC-Durchführung over the cables and insert it into the designated position on the inside of the box.&#x20;

13\. On the outside, pull the Kabeldurchführung over the cables before applying the Mehrfachdurchführung. Make sure the O-Ring is in place. Screw the Kabeldurchführung into its nut.

14\. Attach all cables to the Pixracer. (Power on the designated Pixracer port of the Power Module, GPS cable, UART - Telemetry 2 - connection to the Raspberry PI, Telemetry 1 for the RFD 868 Telemetry)

13\. Screw the Pixracer in place.

14\. Glue the RC Transmitter with double-sided tape to the opposite wall of the Power-Module.&#x20;

15\. Put the Switch through the smaller hole and attach from the outer side the watertight O-Ring sealed Noppen.

16\. Insert the Charing port like shown in following picture:

![](<../../.gitbook/assets/image (28).png>)

17\. Screw the GPS module in the tower. Make sure it is connected to its shielding and to the Pixracer.&#x20;

18\. Connect the RFD 868 Telemetry to the Pixracer (Telemetry 1 port) as well as the Power-Module (power connector at the back of the Power Module)

19\. If every component is in place, solder the Pigtails to the ESC cables.&#x20;

* Remove 4 mm of insulation
* apply solder to the litzen
* heat the pigtail up until solder liquifies
* insert the pre-soldered wire
* add additional solder until the cup is full
* let it cool
* apply a long shrinking tube that the outside of the pigtail is fully covered with shrinking tube

