# Power cables



#### Time required

* **Building:** 30 minutes

#### Tools required

* Soldering iron
* Heatgun

#### Parts required

* Powermodule
* Powercable (3mm diameter)
* Shrink tubing
* ESC Pigtails for motor connection
* ESC
* charging port/connector

## Prepare Power Module

* Presolder the following soldering pads for the upcoming steps

![](../../.gitbook/assets/prepare\_powermodule.jpg)

## Battery -> Power Module

* Cut a 9cm piece of power cable (red and black) - long enough that the battery connctor outside can be connected
* Remove 5mm of isolation on both sides
* Presolder the wires
  * The whole cable should be covered with solder

![Presolder the open ends of the cables](../../.gitbook/assets/presolder\_powercables.jpg)

* Solder the Cables to the corresponding spots on the power module
  * Check your solderings by pulling at them

![Red cable to "BatPlus" - Black cable to "BatMinus"](../../.gitbook/assets/powermodule\_batterycable.jpg)

The Weipu connector can only be connected when the power module is inside the box and the cables are routed through the Weipu connector-hole.

## Power Module -> ESC

* Directly solder the ESC power cables to the designated spots on the power module.&#x20;
* Remove 5mm of isolation on the end of the cable
* Presolder the wires

![Red cable to "PlusESC" - Black cable to "GndESC"](../../.gitbook/assets/powermodule\_esccable.jpg)

## External power plug -> Power Module

* Prepare the connector like this (15cm of cables and shrinking tube)

![](<../../.gitbook/assets/image (30).png>)

* Add the prepared power plug to the power module like seen in the image below (without the irritating excess shrinking tube)

![Insert the two cables like this](../../.gitbook/assets/external\_power\_power\_module.jpg)

* Solder the cables at the back of the power module
  * Add some more solder to the soldering spot to get them soldered perfectly to the board
* Check you solderings by pulling and rotating them
  * No part of the cable should get free

Now your power module should look like this... (shorter cables at the front, shorter connections to the ESC)

![Prepared powermodule with all internal cables soldered](<../../.gitbook/assets/powerboard\_withcables (1).jpg>)

## ESC -> Motor

* Cut 3x 25cm pieces of power cable (red + 2x black) - long enough, that the motor and its pigtals could be later replaced without taking the fuselage apart
* Elongate the three phases of the ESC with these 25 cm long cables.&#x20;
* The pigtails have to be applied after the cables are routed through the "Kabeldurchführung"

![The 3 cables should be long enough, that the connectors can be unplugged outside of the plane](../../.gitbook/assets/img\_20210712\_172201.jpg)



