# Test components before box assembly

1. Add battery to the powermodule connector
2. Test: Power switch
   * Press the switch to turn on the plane internals
   * The Lights of the GPS module should light up
3. Pixracer: Does it work
   * Connect USB Cable
   * Start QGC and check if the Pixracer connects
   * Is SD Card inserted?
   * is logging functional?‌ AHRS
4. Telemetry test:  RFD Connection to QGC

{% hint style="info" %}
QGroundcontrol doesn't have permission to serial port, even when user is part of 'dialout' group? Remove package brltty with: 'sudo apt remove brltty'.
{% endhint %}

* Connect the RFD868+ via USB connector to your PC or the SearchWing-Tablet
* Start QGC and check if it connects to the Pixracer
* Disconnect RFD868+ modem if sucessful

5\. Telemetry test: ESP-Telemetry Connection to QGC

* Connect via PC or SearchWing-Tablet to the drone accesspoint "dronenameRacer" - Password "cassandra"
* Start QGC and check if it connects to the Pixracer

6\. Motor / RC-control

* [ ] Test if RC-Control is working
  * Disable all safty checks using QGC
  * Arm vehicle using the rc-controler&#x20;
  * Throttle up&#x20;
  * Enable all safty checks again using QGC

7\. GPS: Receive > 10 Sats

* Put box outside at clear open sky (no building should obstruct the view)
* Start QGC
* At least 10 sats should be visible

8\. IMU: Receive messages

* Start QGC
* Do dummy calibration to check if IMU messages can be received

9\. Payload test

* Test Camera: PI Can do camera images
  * Check:  [Camera test](https://kidslab.gitbook.io/searchwing/preparing-components/software-setup/setup-companion-computer#camera-image-taking)
  * Check image quality (no blur, no plastic particles)
* Test Payload RX Telemetry / PI Receive Mavlink
  * Check: Check [receive mavlink test](https://kidslab.gitbook.io/searchwing/preparing-components/software-setup/setup-companion-computer#receive-mavlink)
  * Test Payload TX Telemetry / PI Send Mavlink
  * Check: Check [transmit mavlink test](https://kidslab.gitbook.io/searchwing/preparing-components/software-setup/setup-companion-computer#transmit-mavlink) - OR -
  * Check: Check if preflight check msgs in QGC are received
  * Open QGC
  * Wait 1 minute
  * Click "Trumpet" symbol at the top of QGC
  * Check if preflight check messages are received
* Test: DHT22
  * Check: Check [DHT22 tes](https://kidslab.gitbook.io/searchwing/preparing-components/software-setup/setup-companion-computer#dht22)t
