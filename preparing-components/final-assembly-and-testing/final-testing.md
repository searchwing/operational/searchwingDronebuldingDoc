---
description: Do these calibrations and test before the first flight.
---

# Final testing and control

### Configure Power Board Voltage

### Visual Checks

* [ ] Servo Horns (stability check)



### Preperation

* Battery charged (16.8 V)
* Tighten the screws in the wings
* Check for visual defects
  * Klappen Flügel (noch fest?)
  * Klappen V-Tails
  * Servohörner fest?
  * Kamera
  * Membran
  * Deckel auf Ladebuchse?
  * Antenne noch fest?
  * Turm noch fest?
  * Akku-Tüte zu?
* ‌Propeller 10x7" r installed (font in direction of flight) and locked
* ‌SD cards installed and cleared (via download)
* ‌Check M plugs (connecting V tail servos with box)
* ‌check barycentre ("schwerpunkt")
* ‌note weight
* ‌turn QGC, RC, UAV on
* ‌AHRS (Attitude Heading Reference System) check&#x20;
* RC Servo Check
* Flightcomputer parameters
  * Validate that correct parameterset is loaded[ from the FlightComputer Setup](https://kidslab.gitbook.io/searchwing/preparing-components/software-setup/setup-pixracer#load-parameters)
  * Validate that safety parameters are set
* Calibration
  * Accelerometer
  * Compass
  * Level Horizon
  * Baro/Airspeed
* ‌Barometer, GPS check
* ‌Servo trimming
* ‌Save parameter set&#x20;
* ARM DISARM testen
* Motor direction (clockwise)
