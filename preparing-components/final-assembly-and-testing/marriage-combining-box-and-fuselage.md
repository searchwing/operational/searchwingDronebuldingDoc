# "Marriage" - combining box & fuselage

#### Time required

* 30 Minutes work
* 1 day to cure the motor mount glue

#### Tools required

* UHU Por

#### Parts required

* assembled Fuselage halfs

**Check for Defects before continuation**

* are V-Tails glued in?
* are the carbon pipes for Fuselage stabilization glued in?
* is the motor glued in (in one side)?
* are the Servo-Connectors tightly screwed on?
* are all 3 motor connectors correctly attached (rotation direction of the motor)
* are all 3 moter connectors secured with watertight shrink tube?
* are all connectiots (V-Tail Servos, Motor plugs...) connected and secured?&#x20;

## Building Instructions

1. Prepare all parts
2. Apply UHU Por on both parts of the fuselage and the box (see red marks in image below) plus (new!) at the bottom of the box (for additional stability and to reduce water entering)
3. Spread equally and let it dry for 10 minutes until nearly dry
4. Only now put UHU on the motor mount (Yellow below) - it should NOT dry
5. Put the 2 parts together -&#x20;

{% hint style="warning" %}
&#x20;UHU Por is a "contact glue" - you can not move them after pressing them together
{% endhint %}

![](../../.gitbook/assets/assembly-fusselage-glue.jpg)

{% hint style="info" %}
Why use wet glue on the motor mount?&#x20;

The motor mount is inserted sidewards into the fusselage - if the glue is dry, its hard to get it in properly. UHU Por can also be used as a "wet" glue.
{% endhint %}

## Finalization & Testing

* [ ] Check if both parts stick together
* [ ] Check the V-Tails are fixed
*
