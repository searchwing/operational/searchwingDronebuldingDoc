---
description: Test all internal components for correct hardware connectivity
---

# Setup and testing (open box)

## Preparation

* Add battery to the powermodule connector

## Powerboard

* [ ] Test: Powerswitch
  * Press the switch to turn on the plane internals
  * The Lights of the GPS module should light up

## Pixracer and Telemetry

{% hint style="info" %}
QGroundcontrol doesn't have permission to serial port, even when user is part of 'dialout' group? Remove package brltty with: 'sudo apt remove brltty'.
{% endhint %}

* Do the LED's on the Pixracer light up? If not power may not be connected.

<!---->

* [ ] Test: RFD Connection to QGC
  * Connect the RFD868+ via USB connector to your PC or the SearchWing-Tablet
  * Start QGC and check if it connects to the Pixracer
  * Go to application settings -> Mavlink and check loss rate -> should be less than 10 %
    * Is logging functional? -> If not the SD card may be missing.
      * Arm for a few seconds.&#x20;
      *   The incoming message view should show something like this:

          `Logging @SYS/crash_dump.bin`
      * After vehicle reboot and new arm-disarm process a new log should show up
    * Disconnect RFD868+ modem if sucessful
* [ ] Test: ESP-Telemetry Connection to QGC
  * Connect via PC or SearchWing-Tablet to the drone accesspoint "dronenameRacer" - Password "cassandra"
  * Start QGC and check if it connects to the Pixracer

## Motor / RC-control

* [ ] Test: is it Rotating in correct direction / RC-Control is working
  * Add a piece of tape to the motor shaft to see the rotationdirection
  * Disable all safty checks using QGC
  * Arm vehicle using the rc-controler&#x20;
  * Throttle up&#x20;
  * Check rotation of motor: Looking from the back of the plane to the motor, the rotation should be clock-wise
  * If not correct, switch a pair of cables
  * Enable all safty checks again using QGC

## Servos

* [ ] Test: do they rotate?
  * Switch system into manual mode
  * either apply to each M-Plug a servo or use a single one and test each connection on its own&#x20;
  * Use RC Controller to check rotation - trimming will be set later

## GPS

* [ ] Test: Receive > 10 Sats
  * Put box outside at clear open sky (no building should obstruct the view)
  * Start QGC
  * At least 10 sats should be visible

## External compass

* [ ] Test: Compass calibration healthy
  * Start QGroundControl
  * Go to Vehicle Setup -> Sensors
  * Start compass calibration for external compass
  * Make sure the compass calibration results in a healthy compass (green)&#x20;

## IMU

* [ ] Test: Receive messages
  * Start QGC
  * Do dummy calibration to check if IMU messages can be received

## Payload

* [ ] Camera cables do not cross each other
* [ ] Camera orientation correct: cable connector facing to front of plane
  * [ ] Check that left camera image is showing really left side of the drone&#x20;
  * [ ] Check that right camera image is showing really right side of the drone
  * [ ] Top border of the images are pointing towards flying direction of the drone
  * [ ] Bottom border of the images are pointing to the back of the drone
* [ ] Test Camera cables:
  * [ ] Camera cable glued to PI
  * [ ] Camera cable glued to left camera
  * [ ] Camera cable glued to right camera
* [ ] Test Camera: Removed camera lens protection
  * Ensure that the green protection film is removed
* [ ] Test Camera: PI Can do camera images
  * Check: Check [Camera test](https://kidslab.gitbook.io/searchwing/preparing-components/software-setup/setup-companion-computer#camera-image-taking)
  * Check image quality (no blur, no plastic particles)
* [ ] Test Payload RX Telemetry / PI Receive Mavlink
  * Check: Check [receive mavlink test](https://kidslab.gitbook.io/searchwing/preparing-components/software-setup/setup-companion-computer#receive-mavlink)
* [ ] Test Payload TX Telemetry / PI Send Mavlink
  * Check: Check [transmit mavlink test](https://kidslab.gitbook.io/searchwing/preparing-components/software-setup/setup-companion-computer#transmit-mavlink) - OR -
  * Check: Check if preflight check msgs in QGC are received
  * Open QGC
  * Wait 1 minute
  * Click "Trumpet" symbol at the top of QGC
  * Check if preflight check messages are received
* [ ] Test: DHT22
  * Check: Check [DHT22 tes](https://kidslab.gitbook.io/searchwing/preparing-components/software-setup/setup-companion-computer#dht22)t



## ESC Calibration

{% hint style="warning" %}
ESC has to be calibrated before closing the box!
{% endhint %}

* Turn power off (main switch)
* connect PixRacer with USB to a computer
* Set to `manual` mode&#x20;
* Arm&#x20;
* raise throttle to max on the RC
* Turn power on (main switch) -> ESC peeps (with motor)
* lower throttle to min -> ESC peeps once
* Done.



{% embed url="https://ardupilot.org/plane/docs/guide-esc-calibration.html" %}



## Finalization & Testing

* [ ] check this
* [ ] and that...
