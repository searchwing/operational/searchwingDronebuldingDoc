# Batteries

## Requirements

* LiPo battery
* Titanex power cable 2.5 mm²
* shrinking tube
* Weipu connector
* balancer board
* PlastiDip

### Tools

* label printer

## Safety

When working on the batterey terminals take care to not short circuit the battery. **Always tape the loose end if you work on another end**

## Aseembly

Prior to assembly make sure the battery works and has proper voltage levels.

### Add basic information

Use a label printer and add a label with the minimum and maximum battery voltage to the battery.

Add a unique identifying label to the battery. Suggestion: "\<dronename>-01".

### Add Weipu connector

...

### PlastiDip application

...

## Testing

Make sure the Weipu connector is wired correctly.

Pin 1 should be XXX.

Pin 2 should be XXX.
