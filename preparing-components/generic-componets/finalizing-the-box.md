# Finalizing the Box

#### Time required

* **Preparation & Work:** 1-2 hours
* **Cure time:** approx. 24 hours&#x20;

#### Tools required

* Clamps to fix the parts
* silicon gloves
* mixing rod
* mixing container
* brush
* plastik wrap

#### Parts required

* Box parts from "[Printing the box](printing-and-preparing-the-box.md)"
* SuperGlue
* 2-Component Epoxy - [https://shop1.r-g.de/art/100100](https://shop1.r-g.de/art/100100)

## Overview

* Glue the box parts together (15 min)
* Glue the holders to the box (15 min)
* coat the box, cover and GPS tower with epoxy
* let it cure for 24h / temper if possible

## Glueing the box

<img src="../../.gitbook/assets/grafik.png" alt="" data-size="original">

### Box Parts

Glue the 2 parts of the box together

![](../../.gitbook/assets/BoxParts-glue.png)

### Box Holders

Use the additional positioning help part to put the 2 holders in the right position

{% hint style="danger" %}
Make sure to use the right smaller part - there is one for the left and one for the right side
{% endhint %}

![](../../.gitbook/assets/IMG\_5924.jpg)![](../../.gitbook/assets/IMG\_5925.jpg)

## Coating the parts

* Prepare all parts to be coated:
  * Box with attached holders
  * cover
  * GPS Tower
* Prepare your tools
  * Brush
  * Gloves&#x20;
  * Container for mixing the epoxy
* Prepare your workplace
  * use a board to place the coated parts - cover it with plastic wrap

{% hint style="info" %}
Make sure to mix the right ratio of the epoxy. Best to use weight-ratio with a exact mg scale.
{% endhint %}

Use 12g of mix for all parts. 6g Harz, 6g Härter.

{% hint style="info" %}
After mixing the epoxy, spread it out in a larger container! Epoxy gets hot while curing, the hotter it gets, the faster it cures. Normal c
{% endhint %}

### &#x20;

![](<../../.gitbook/assets/RG Epoxid Hinweise.png>)

#### Applying the coat

* Apply the coat with a brush
* hold the box on the holders while applying
* apply on all parts - the thinner the better!
* dont let the epoxy fill up the holes or the grove for the cover

![](../../.gitbook/assets/ApplyEpoxy.png)

### Curing

* let the exopy cure for 24h
* if possible, you can "temper" the parts while curing, increasing the temperature during the curing increases strength of epoxy ande decreases curing time - see below
  * Curing can e.g. be done in the heated box of your 3D printer. Set the heating plate to a low temperature e.g. 50 degree and put the parts in the print chamber.

## Additional ressources

{% embed url="https://www.r-g.de/wiki/Harze:Die_wichtigsten_Bedingungen_f%C3%BCr_eine_erfolgreiche_Verarbeitung_von_Epoxidharzen" %}

{% embed url="https://www.google.com/url?cd=&esrc=s&q=&rct=j&sa=t&source=web&url=https%3A%2F%2Fwww.swiss-composite.ch%2Fpdf%2Fi-tempern.pdf&usg=AOvVaw0pu-LWJLDjqZ3dKY-k_oJg&ved=2ahUKEwi4k4Cwtrj0AhUxhP0HHSNuBU4QFnoECAIQAQ" %}
