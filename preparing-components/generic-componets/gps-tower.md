---
description: Building the GPS Tower
---

# GPS Tower

## Requirements

### Parts

* GPS tower/top cover of box
* M9N GPS module
* 6-PIN JST cable
* 4 x screws
* "styropor" block
* copper tape
* plastic foil
* electrical tape

### Tools

* cross-head screw driver
* scissor&#x20;

## Assembly of the tower

Screw in the GPS m9n Module. Direction doesn't matter. Case is symmetrical.

![](../../.gitbook/assets/gps\_tower\_1.jpeg)

Fit GPS cable through the hole in the cover and plug it in the designated port on the pixracer.

![](../../.gitbook/assets/gps\_tower\_2\_1.jpeg)

#### Electromagnetic shielding

Prepare a styropor cube of the size: 50x40x30mm and cover the bottom side with Alu or Copper foil.&#x20;

![](../../.gitbook/assets/gps\_tower\_3.jpeg)

To prevent short circuits wrap it with some plastic foil.&#x20;

![](<../../.gitbook/assets/gps\_tower\_4 (1).jpeg>)

Here are some leftovers from the vacuum packages from the battery bag used.

![](../../.gitbook/assets/gps\_tower\_5.jpeg)

Put the cube in the GPS tower.

## GPS Test

Before glueing of the Tower check if the GPS gets an appropriate amount of Sattelite locks (>15 is good).

Check if the external compass works.





