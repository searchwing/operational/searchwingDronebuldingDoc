---
description: >-
  To connect the servos for ruders to the box, containing all the electronics,
  we use M8 connectors. These offer a small footprint and the required
  watertightness
---

# M8-Servo-Plugs

### Remarks for new M8 connects

#### Vtails

* V-Tail to PixRacer internal M8 cable length: 24 cm
* Wing to PixRacer internal M8 cable length: 14 cm&#x20;
* Cable code: blue signal, brown 5V, black GND
* Cut M8 pre-made cable to 7 cm for V-tail
* Cut the servo cable to 16 cm for the V-tail

####

Aileron

* Cut the M8 Cable to 37 cm
* remove about 15 mm outer isolation and 3 mm inner isolation
* Route it through the designated cable hole in the fuselage, and attach it to a counterpart which is mounted in a box.
* Insert the servo motor into the wing and hold the wing to the fuselage

![](<../../.gitbook/assets/image (19).png>)

* if you are brave, cut the servo cable at the beginning of the uninsulated copper of the other cable
* solder them together and apply shrinking tube









Final Product

* 2x F1
* 2x F2
* 4x F3

![All servo cable assemblies for one plane](../../.gitbook/assets/servocable\_finalcableset.png)

## Requirements&#x20;

### Time required

* **Complete cable assebly set:** **170 min**
  * Preparation: 30 min
  * Building F1: 60 min
  * Building F2: 40 min
  * Building F3: 40 min

### Tools required



![](../../.gitbook/assets/servocable\_toolsrequired.png)



### Parts required

* [Prepared Servos](untitled-1.md)&#x20;
* 4x&#x20;
* 4x&#x20;

![](../../.gitbook/assets/servocable\_stueckliste.png)



### Preparation

Schrumpfschlauch und Kabel ablängen&#x20;

![](../../.gitbook/assets/servocable\_schrumpfschlauch.png)

## Step by Step Guide

####

####

### F1

![](../../.gitbook/assets/servocable\_step4step\_wingservo.png)

![](../../.gitbook/assets/servocable\_step4step\_m5wing.png)

### F2

![](../../.gitbook/assets/servocable\_step4step\_wing\_m5.png)

### F3

![](../../.gitbook/assets/servocable\_step4step\_insidebox.png)

###

## P**in assignment**

![](../../.gitbook/assets/servocable\_pinbelegung2.png)

## **Tips and Tricks**

![](../../.gitbook/assets/servocable\_parallelwire.png)



![](../../.gitbook/assets/servocable\_heisluftfoen.jpg)

![](../../.gitbook/assets/servocable\_solderpin.png)

![](../../.gitbook/assets/servocable\_soldertip.png)



![](../../.gitbook/assets/servocable\_stripax.png)





## Finalization & Testing



* [ ] check this
* [ ] and that...



#### Outdated Manual:&#x20;

{% embed url="https://www.hs-augsburg.de/homes/beckmanf/dokuwiki/doku.php?id=searchwing-wing-servo&s[]=m5" %}

