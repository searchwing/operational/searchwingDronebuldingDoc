# Motor assembly and motor mount preparation

### Required

* Motor marked with "WP" (waterproof)
* 4 screws M3x6mm
* 4 screws cylindric head
* 4 non-metric? screw for motor mount
* 4 screw nuts
* motor cross
* 3D-printed motor mount
* **Loctite for all screws!**

### Motor cross and propeller axis

* Screw motor cross to motor (M3x6mm, PH1, metric with countersunk head)

![](../../.gitbook/assets/motor\_crossmount\_prior\_to\_assembly.jpg)

* screw propeller axis to motor

![](../../.gitbook/assets/motor\_axel\_prior\_to\_assembly.jpg)

### Motor mount

* Press nuts in motor mount (heat nuts and use tool)

![](../../.gitbook/assets/motormount\_nuts\_inserted.jpg)

* screw motor cross to motor mount (M3x10mm, PZ1, not metric?)

![](../../.gitbook/assets/motor\_mounted\_to\_motormount.jpg)

