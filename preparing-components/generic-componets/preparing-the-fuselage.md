# Preparing the fuselage



#### Time required

* **Building:** 30 minutes

#### Tools required

* UHU Por glue
* spatula
* cutter/knife

#### Parts required

* carbon rods
* both fuselage parts
* 3D printed motor mount

## Installing carbon fuselage rods

The glue has to dry for 10 minutes before fitting the parts together. So apply UHU por glue to the carbon rods and the fuselage part where the rods are inserted. Wait 10 minutes. Only then press the rods into the slots in the fuselage.

Do the same for the V-tail rods.

![](../../.gitbook/assets/fuselage\_carbon\_rods.jpg)

## Installing the V-Tails to the fuselage

When all the rods are glued, install the servos and test fitting the servo connectors through the fuselage. Do not use the servo hinges included (to be glued) but take our custom ones to screw.

## Installing the motor mount

Apply UHU Por glue to the first half of the fuselage:

![](../../.gitbook/assets/fuselage\_glue\_for\_motormount.jpg)

Apply glue to half of the motor mount:

![](../../.gitbook/assets/motormount\_with\_glue.jpg)

Wait for 10 minutes and press motor mount firmly against the fuselage.

![motor mount glued to fuselage](../../.gitbook/assets/motormount\_glued.jpg)

## Cutting the opening for the cameras

First, cut of the rear "hook" symetrically (as a 'pyramide').

![](<../../.gitbook/assets/image (31) (1).png>)

Use the box to mark the area of the bottom of the fuselage below the wings that has to be cut out. It's a width of about 35mm (to both side of the center, inside and outside of the bottom of the fuselage) with a length of about 70mm from the front edge of the narrow part of the bottom of the fuselage (outside). The camera bulge of the box is narrower (2.5cm inside the fuselage bottom) but they need some opening angle (about 60 deg) to both sides to take proper pictures, so the opening widens from 2.5cm inside to 3.5cm outside. It might be advantageous to cover the entire contact area of box and fuselage with glue when glueing together in a later step.

![](<../../.gitbook/assets/image (26).png>)





## Finalization & Testing



* [ ] check this
* [ ] and that...
