# Printing the box

#### Time required

* **Preparation:** 30 Minutes
* **Print Time:** approx. 15 hours&#x20;

****

#### Tools required

* 3D-Printer (Prusa recommended)

#### Parts required

* ASA Filament
* SuperGlue
* 2-Component Epoxy&#x20;

## The right printing material

We choose ASA ([DuraPro ASA](https://www.extrudr.com/de/produkte/catalogue/asa-durapro-neon-orange\_2989/)) as our printing material of choice:

* **Light** (approx. 30% lighter than PETG)
* **Stable** - good layer adhesion (compared to ABS)



## Settings for the slicing / printing

{% hint style="info" %}
The complete config bundle for the PrusaSlicer, most current STL Files ande more can be downloaded here:
{% endhint %}

{% embed url="https://gitlab.com/searchwing/development/TheBox" %}

The brim is important, when printing with ASA. ASA tends to warp and contract - this holds the print better to the plate.

![](../../.gitbook/assets/searchwing-box-printing-brim.png)

Density is set to 100% - the box is designed, to print in 4+ perimeters, this makes sure, all the cavities are filled.&#x20;

![](../../.gitbook/assets/searchwing-box-printig-infill.png)

## Printing the parts



### Box - Front

![](../../.gitbook/assets/searchwing-box-printing-orientation-2.png)

### Box - Back

![](../../.gitbook/assets/searchwing-box-printing-orientation-1.png)

### Wing-Connectors

### Box - Cover

## Glue the holders to the box & box parts together



## Glue camera glasses to the box

## Finalization & Testing



* [ ] Check the glue joints
* [ ] Check the silicon seals
* [ ] Check that cover fits on box!
