---
description: To connect different components, special cables have to be produced.
---

# Producing special cables

#### Time required

* **Building:** 30 minutes

#### Tools required

* soldering iron
* heat shrink tube

#### Parts required

for DHT:

* 3x Cable ca. 65mm (red, yellow, black)
* 1x Cable ca. 30mm&#x20;
* 1x 4.7 kOhm (yellow)

**JST Pinout**

![From https://bluerobotics.com/learn/wl-connector-standard/jst-gh-pin-identification/](<../../.gitbook/assets/image (19) (1).png>)

## Power-Module -> CM4

This cable connects the power from the power module to the CM4. Use the JST GHT and the fitting, crimped cables and put it together. There is a +5V supply on pin 1&2 and ground on pin 3&4. We combine each of the two pins to get enough power for the CM4.

![](<../../.gitbook/assets/image (26) (1).png>)

![Cable length should be \~17cm.](<../../.gitbook/assets/image (27).png>)

## DHT22 -> CM4

&#x20;This cable also connects the DHT22 sensor (humidity and temperature) to one pi.

#### DHT22 -> Raspberry Pi Zero

To meassure the air temperature and humidity on the inside of the Box, a DHT22 sensor needs to be attached to one of the Pi's.&#x20;

![DHT pin layout (the uncolored pin is not used)](<../../.gitbook/assets/image (1).png>)

![scematic for connection](../../.gitbook/assets/image.png)

1. First solder (red) vcc cable and the resistor to the DHT as shown

![](../../.gitbook/assets/img\_20210607\_140835.jpg)

2\. Solder the 20mm cable to the data Pin and shrink the first DHT pin.

![](../../.gitbook/assets/img\_20210607\_141408.jpg)

3\. Solder the open end of the resistor, the open end of the 20mm cable and the long data cable (yellow) together.

![](../../.gitbook/assets/img\_20210607\_141638.jpg)



4\. Attach the (black) GND cable to the last Pin, and cover your bad soldering points with shrinking tube.

![](../../.gitbook/assets/img\_20210607\_142049.jpg)

![Finished dht22 cable with connectors for cm4](<../../.gitbook/assets/image (25).png>)

<img src="../../.gitbook/assets/file.drawing.svg" alt="" class="gitbook-drawing">

## Power Switch -> Power Module

* Solder two precrimped JST-GH cables (\~20cm long) to the power switch as follows

![](<../../.gitbook/assets/powerswitch\_soldered (2) (2) (2).jpg>)

Insert the cable ends into the JST connector. Insert at pin 1 and pin 4 of the JST connector.

* Isolate the cables at the switch side
  * Using hot glue
  * !! Special caution needed on the lenght of the isolation at the switch side !!

![Isolate the cables not above the switch connectors ... ](<../../.gitbook/assets/powerswitch\_solderednear (3) (3) (3).jpg>)

![.... otherwise you could get some trouble with the pixracer connectors - they might block them](<../../.gitbook/assets/powerswitch\_pixracer (5) (2) (2).jpg>)

## PixRacer -> CM4  Telemetry

The next step is to build the cable for the telemetry connection between Pixracer and the CM4. Therefore the JST GHT is needed. Moreover you need a wire, because each Pi needs to be connected to the PixRacer. There need to be 3 wires in the JST GHT.&#x20;

<img src="../../.gitbook/assets/file.drawing (2).svg" alt="CM4 to JST-GH Serial " class="gitbook-drawing">

![](<../../.gitbook/assets/image (29).png>)

## PixRacer -> RC-Receiver&#x20;

The cable connects receiver in the plane ([FrSky R-XSR](../../parts/list-of-parts-and-where-to-buy.md#receiver)) to 2 ports ob the PixRacer: **RCIN** and **FRS**.

![All cables needed](../../.gitbook/assets/frsky-receiver-cable-needed-cables.jpg)

You need:

* From FrSky-Receiver:
  * Plug with 5 cables
  * Remove the white one
* From PixRacer:
  *   JST-5 connections (fitting the RCIN connection)

      JST-4 connections (fitting the FRS connection)

**Cable must be drilled!**

![](../../.gitbook/assets/frsky-receiver-cable-wiring.png)

![](../../.gitbook/assets/rc\_transciever\_to\_pixracer.jpeg)

#### How to test

* connect to PixRacer
* connect to receiver
* [bind the remote control to receiver](../software-setup/receiver-software.md#bind-the-receiver-the-the-rc)
* check connection
* check telemetry&#x20;
  * Long Press on "PAGE"
  * See the telemetry from the PixRacer

## RFD 868+ telemetry radio  -> PixRacer

from [https://www.hs-augsburg.de/homes/beckmanf/dokuwiki/doku.php?id=searchwing-rfd868-mount](https://www.hs-augsburg.de/homes/beckmanf/dokuwiki/doku.php?id=searchwing-rfd868-mount)

The modem is connected to the PixRacer Telem2 port and the power module. The modem is mounted in the right fuselage half and will be connected just shortly before glueing the two fuselage parts to the PixRacer and the Powermodule.

### UART cable to PixRacer

* Use three 12inch precrimped JST-GH cables
* Remove the JST-GH crimp at one side
* Remove 3-4mm of isolation to crimp a Harwin M20 crimp
* Crimp a Harwin M20 connector
* Use a Harwin 8 pin connector and put the crimps in the connector according to the following image

![](<../../.gitbook/assets/image (19) (1) (1).png>)

Compare the connector to the RFD868+ connections at the modem.

![](<../../.gitbook/assets/image (29) (1).png>)

Remove Pin 15 of the RFD868+ modem. This will be the safety pin to avoid wrong mounting of the connector.

![](<../../.gitbook/assets/image (30) (1).png>)

Solder the pin in a Harwin M20 connector and insert that connector in the Harwin housing. Fill the other four open places with empty Harwin M20 connector to increase the holding force of the connector.

Mark the RX and TX and GND cables and then weave the cables such that they are closer together. Now take a JST-GH 6-Pin connector and insert the pins according to this image

![](<../../.gitbook/assets/image (21) (1).png>)

Pixracer pin layout: [https://docs.px4.io/v1.9.0/assets/flight\_controller/pixracer/grau\_setup\_pixracer\_top.jpg ](https://docs.px4.io/v1.9.0/assets/flight\_controller/pixracer/grau\_setup\_pixracer\_top.jpg) &#x20;

Please note the different directions of the connector/cables in the labeling.

Pin layout of the Pixracer:

![](<../../.gitbook/assets/image (24) (1).png>)

Pin layout to the RFD868+:

![](<../../.gitbook/assets/image (22).png>)



**Note the difference between the yellow cable named TX on the board and RX on the connector!!! The name RX for the yellow cable on the JST-GH cable connector refers to the RX on the RFD868 modem.** Please remember to connect the RFD868+ TX to the PixRacer RX and vice versa. So according to the following table:

| PixRacer JST-GH board | RFD868+ IDC connector |
| --------------------- | --------------------- |
| GND                   | GND                   |
| RX                    | TX                    |
| TX                    | RX                    |



The electrical connections of the cable are shown here:

![](<../../.gitbook/assets/image (20).png>)

The final weaved cable looks like this:

![](<../../.gitbook/assets/image (27) (1) (1).png>)

You can remove the cable markings.

### Power Module to RFD868+ modem&#x20;

* 4x 8" JST already crimped cable
* JST 4-pin to female connector 8-pin
* GND + GND, 5V + 5V (double cable) &#x20;

![](<../../.gitbook/assets/image (28) (1).png>)

To mark the cable use colored shrink tube.



## Finalization & Testing



* [ ] Connect Pixracer, Pis and Power Module&#x20;
* [ ] Pi receives messages from pixracer&#x20;
* [ ] Pi measures DHT22 humidity

