---
description: >-
  The motor itself is waterproof in general. The only weak points are the
  bearings - the original bearings are vulnerable to water, especially salt
  water.
---

# Waterproofing the motor

#### Time required

* **Building:** 10 minutes

#### Tools required

* 3D-printed mounting aid (export the STL data from the [CAD modell](https://cad.onshape.com/documents/3d72292578f0cf87124dafaf/w/645e27e78913f8a2fc46f6eb/e/d92f338c5cb887f88ac1b3e7))
* cotter pin drive&#x20;
* hammer&#x20;
* small screwdriver
* Circlip Plier for circlip on motor shaft ([Knipex 46 11 A0](https://www.knipex.com/en-uk/products/circlip-pliers/circlip-pliers-for-external-circlips-on-shafts/circlip-pliersfor-external-circlips-shafts/4611A0))

#### Parts required for waterproofing one motor

* 1x Extron Motor
* 2x bearing small&#x20;
* 1x bearing big&#x20;
* 1x replacement circlip for 5mm motor shaft

## Step 1: disassemble the motor

First of all the circlip shown in the picture below needs to be removed. Therefore a small screwdriver could be helpful.&#x20;

![](../../.gitbook/assets/motorlagerwechsel-sprengring-entfernen.jpg)

After removing the circlip the motor can easily pulled apart. For the next steps you only need to modify the part of the motor, shown in the right side of the next picture.&#x20;

![](../../.gitbook/assets/motorlagerwechsel-auseinandergebaut.jpg)

There are two little spacer made out of brass on the big bearing on the right side of the picture. You will need the spacer, so don't lose them.&#x20;

## Step 2: Removing the bearings

The motor has two small and one bigger bearing which needs to be replaced. You should start with removing the two small bearings first. Therefor place the motor in the mounting aid. You can use your cotter pin driver and position it as shown in the following picture. Use a hammer to cast out the bearing.&#x20;

![That's how the cotter pin driver should be sited on the bearing  ](../../.gitbook/assets/motorlagerwechsel-platzierung-austreiber.jpg)

![Removing the two small bearings ](<../../.gitbook/assets/motorlagerwechsel-kleine-lager-austreiben (1).jpg>)



After dismounting the two small bearings turn the motor and the mounting aid around and place it like shown in the next picture. Remove the big bearing by the same method used for the smaller ones.&#x20;

![Removing the big bearing](../../.gitbook/assets/motorlagerwechsel-grosses-lager-austreiben.jpg)



## Step 3: Integrating the new bearings&#x20;

You are now able to integrate the new bearings. Start with the two small bearings and use the mounting aid, so that the bearings and the motor stays in the right position won't get damaged by the hammer. \
After putting in the two small bearings flip the motor around and install the big bearing with the bigger mounting aid.

![integrating the bearings using the mounting aid](../../.gitbook/assets/motorlagerwechsel-lager-einbauen.jpg)

![integrating the bearings using the mounting aid](../../.gitbook/assets/motorlagerwechsel-grosses-lager-einbauen.jpg)





## Step 4: assemble the motor

After installing the new bearings you are now able to reassemble the motor. Don't forget to place the two little spacer made out of brass on the shaft before adding the circlip. The two motor parts move quite fast due to the magnets - **make sure you do not have parts of your skin between the two parts...**

![motor with new bearings](../../.gitbook/assets/motorlagerwechsel-neue-lager-eingebauter-zustand.jpg)

## Finalization & Testing



* [ ] check this
* [ ] and that...
