# Create model on RC

## Requirements

* X9D+ RC controller

## Create model

* Switch on RC
* If Schalter fehlpositioniert: switch SD up
* Press Menu
* Use +/- to navigate to Billie
* Press Enter
* Navigate to 'Kopiere Model'
* Press Enter
* Use +/- to navigate to free slot
* Press Enter
* Navigate to Billie copy
* Press Enter
* 'Modell auswaehlen'
* Press Enter
* Press Page
* change model name
* go down to 'Empfaenger Nummer'
* change 'Empfaenger Nummer' to slot number (number in front of model name)

Configuration of the RC is possible through OpenTX companion software too.
