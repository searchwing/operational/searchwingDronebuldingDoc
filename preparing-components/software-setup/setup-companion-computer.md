# Setup payload Pi Computers

#### Time required

* &#x20;**** 30 Minutes

#### Parts required

* 1xWaveshare Board CM4-IO-BASE-B
* 1xRaspberry CM4 Board
* 1xUSB-C Cable
* 1xSW-AP1 Router (for wifi login)

## Flash image to board

* &#x20;Download image: [http://breakout.hs-augsburg.de/ciartifacts/companion/cm4-waveshare/](http://breakout.hs-augsburg.de/ciartifacts/companion/cm4-waveshare/)
* &#x20;Flash image to SD Card&#x20;
  * e.g. using Etcher Tool [https://www.balena.io/etcher/](https://www.balena.io/etcher/)
  * or using dd in \*NIX systems [https://www.raspberrypi.org/documentation/installation/installing-images/linux.md](https://www.raspberrypi.org/documentation/installation/installing-images/linux.md)

## Configure

* Login to pi via ssh to the default dronename from the image:

```
$ ssh searchwing@searchwing-drone
```

or via IP (get from router)

```
$ ssh searchwing@x.y.z.w
```

* Do first boot setup:
  * Dronename is set in this script
  * Partition /data is resized to full disk size

```
$ sudo ./first_boot_setup.sh
```

## Testing

### Login

* Connect cameras with thin camera cables
* &#x20;You can access each camera via its hostname:&#x20;

```bash
$ ssh searchwing@perlman
```

or

```bash
$ ssh searchwing@perlman.local
```

### Camera image taking

* Check for new images using pi webserver
  * Open webbrowser
  * Go to`http:\\perlman`
  * Click on `0_latest.jpg@`
  * The most recent image should appear (and can be refreshed using browser refresh / F5-key)
  * You can also check if new images appear in the main list
* For debugging you check output of the image payload:&#x20;

```bash
$ journalctl -f -u searchwing_payload.service
```

* If the progress is stopped, most probably the pi camera cable isnt working properly

### DHT22

* Currently no ROS payload is available so we need to test by hand (from [https://learn.adafruit.com/dht-humidity-sensing-on-raspberry-pi-with-gdocs-logging/python-setup](https://learn.adafruit.com/dht-humidity-sensing-on-raspberry-pi-with-gdocs-logging/python-setup))
* Run testdh22.py

```bash
$ python /home/searchwing/testdh22.py 
A full buffer was not returned. Try again.
Temp: 67.1 F / 19.5 C    Humidity: 38.9% 
Checksum did not validate. Try again.
A full buffer was not returned. Try again.
Temp: 66.6 F / 19.2 C    Humidity: 41.5% 
Temp: 66.7 F / 19.3 C    Humidity: 46.7% 
A full buffer was not returned. Try again.
A full buffer was not returned. Try again.
Checksum did not validate. Try again.
Temp: 76.8 F / 24.9 C    Humidity: 99.9% 
Temp: 77.4 F / 25.2 C    Humidity: 99.9% 

```

### Flightcontroler / Pixracer Mavlink connection

* Stop payload as it uses serial port already

```
$ sudo systemctl stop searchwing_payload
```

* Connect to the flightcontroller via mavproxy

```bash
$ mavproxy.py --master=/dev/ttyS0,57600
```

#### Receive Mavlink

* After a few seconds, the connection should be established

```bash
Connect tcp:127.0.0.1:5760 source_system=255
Log Directory: 
Telemetry log: mav.tlog
Waiting for heartbeat from tcp:127.0.0.1:5760
MAV> online system 1
RTL> Mode RTL
```

#### Transmit Mavlink

* On the Pi with RX/TX connected to the pixracer you can now test TX&#x20;
  * Set the plane to Manual Mode
  * Set `mode MANUAL by writing to the prompt:`

```
mode MANUAL 
```

* Observe if the output is as following:

```bash
RTL> Got COMMAND_ACK: DO_SET_MODE: ACCEPTED 
MANUAL> Mode MANUAL
```

* The plane should be now in manual mode

#### Debugging (deprecated)

*   using searchwing-pi repo / searchwing-payload-camera

    * set `LOGGER_LEVEL = "DEBUG"`in `/home/searchwing/searchwing-pi/configuration.toml`
    * restart: `sudo systemctl restart searchwing-payloads-camera.service`
    * Check output

    ```bash
    $ journalctl -f -u searchwing-payloads-camera.service
    ```
*   using mavlink-router stats per port

    * enable mavlink-router stats printing in `/etc/mavlink-router/config.d/main.conf`&#x20;

    `[General]`

    **`ReportStats=true`**

    `MavlinkDialect=auto`

    * restart mavlink-router: `sudo systemctl restart mavlink-router.service`
    * Check output

    ```bash
    $ journalctl -f -u mavlink-router.service
    ```

