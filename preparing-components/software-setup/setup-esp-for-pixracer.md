# Setup ESP for Pixracer

## Requirements

* PixRacer with ArduPilot installed
* Micro USB cable
* External power supply
* ESP
* Laptop with QGroundControl

## Flash MAVESP8266

* Use hints from [https://ardupilot.org/copter/docs/common-esp8266-telemetry.html](https://ardupilot.org/copter/docs/common-esp8266-telemetry.html)

Linux users can use the commandline if the ESP is connected to a serial adapter:

```
esptool.py --chip auto --port /dev/ttyUSB0 --before no_reset  write_flash --flash_mode dio --flash_size detect --flash_freq 40m 0x0000 firmware-esp01_1m.bin
```

## Install it to the pixracer

* Insert the esp as seen below into the pixracer

![](<../../.gitbook/assets/image (4) (2) (2).png>)



## Configure&#x20;

* connect from your PC to the wifi access point (initial access point ID is either “ArduPilot”  with password "ardupilot" or "Pixracer" with password “pixracer”)
* open a browser to [192.168.4.1](http://192.168.4.1/) and a simple web interface will appear&#x20;
* click on the “Setup” link
* set config&#x20;
  * AP SSID := dronenameRacer (replace "dronename" with your real drone name)
  * AP Password := cassandra
  * baudrate := 921600
* push the “Save” button and reboot the device

## Testing

* Power the PixRacer with an external power supply (do not connect USB to your PC)
* Connect to the new WiFi with name "\<drone\_name>Racer"
* Open QGroundControl and check if you receive telemetry data via WiFi
* Open the "Vehicle Setup" menu and check if you can configure the ESP through the ESP configuration menu&#x20;
