# Setup Groundcontrol Laptop

## Load latest image to the laptop

* Download latest clonezilla image from [https://cloud.hs-augsburg.de/s/gPpRWP6r69FqyZp](https://cloud.hs-augsburg.de/s/gPpRWP6r69FqyZp)
* Install image to the laptop using this instructions [https://clonezilla.org/show-live-doc-content.php?topic=clonezilla-live/doc/02\_Restore\_disk\_image](https://clonezilla.org/show-live-doc-content.php?topic=clonezilla-live/doc/02\_Restore\_disk\_image)
  * Special settings for our ASUS Laptop&#x20;
    * boot from USB stick
    * In Clonezilla Menue Choose "Other Modes" then "KMS"

## How to use

* How to plan a mission see [https://app.gitbook.com/o/-MWAbYktIOVCqPHoTxRn/s/-MYsj38KDFg4fXvhUh7B/c/pfT6bXQh6gA0kGecJS7e/using-the-drone/untitled](../../using-the-drone/untitled.md)
* How to analyze images using the Laptop see [https://app.gitbook.com/o/-MWAbYktIOVCqPHoTxRn/s/-MYsj38KDFg4fXvhUh7B/c/pfT6bXQh6gA0kGecJS7e/using-the-drone/after-flight](../../using-the-drone/after-flight.md)&#x20;





