# Setup Pixracer

## Requirements

* Pixracer [**firmware version 4.0.9**](https://firmware.ardupilot.org/Plane/)****
* Micro USB cable
* qGroundControl installed
* SD card 1 to 8 GB
* time: 30 min

## Load parameters

* Download parameters from Gitlab
  * [https://gitlab.com/searchwing/operational/searchwing-config/-/blob/master/ardupilot/v4.0.3/LATEST.params](https://gitlab.com/searchwing/operational/searchwing-config/-/blob/master/ardupilot/v4.0.3/LATEST.params)
* Upload Parameters
  * Start QGC
  * connect Pixracer via USB
  * Go to Menu->VehicleSetup->Parameters->Tools->Load from File
  * Choose file "LATEST.params"
  * do not panic! confirm need to reboot (many times)
  * Reboot via Tools->Reboot
* TODO

### Format SD Card to FAT32

**On Linux:**

1. identify device with `lsblk` this example will use `sda`
2.  create partition table with: &#x20;

    `sudo parted /dev/sda --script -- mklabel msdos`&#x20;
3.  create partition with 100% FAT32:

    `sudo parted /dev/sda --script -- mkpart primary fat32 1MiB 100%`
4.  Format partition:

    `sudo mkfs.vfat -F32 /dev/sda1`

**On Windows:**

1. right-click on device and use GUI

### Configure SYSID\_THISMAV

Configure the SYSID\_THISMAV parameter with QGC:

* Go to Menu -> VehicleSetup -> Parameters
* Search for SYSID\_THISMAV
* Configure a unique SYSID
* Document usage in the table below

#### SYSIDs:

| UAV   | SYSID |
| ----- | ----- |
| Maggy | 1     |
| Elly  | 2     |
