---
description: These settings apply to QGC running on Tablets or Laptops
---

# Setup QGroundControl

{% hint style="info" %}
QGroundControl doesn't have permission to serial port, even when user is part of 'dialout' group? Remove package brltty with: 'sudo apt remove brltty'.
{% endhint %}

## Go To Location distance limitation

* Default allowed maximum range from the plane to next Go To Location is 1000m
* Set it to a more reasonable value
  * Open "Application Settings"
  * Goto "General" section
  * Set "Got To Location Max Distance" to 50000m or more

<img src="../../.gitbook/assets/image (19) (1) (1) (1) (1).png" alt="" data-size="original">

### Enable Telemetry log saving

* Open "Application Settings"
* Goto "General" section
* Set suitable path
* Enable the red marked entrys

![](<../../.gitbook/assets/image (27) (1).png>)
