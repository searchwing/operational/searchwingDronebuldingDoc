# Setup RFD868+ radio

### Requirements

* RFD868+ radio
* USB to serial adapter for RFD868+ radio
* Laptop with minicom or RFDDesign setup tool

{% hint style="info" %}
QGroundcontrol doesn't have permission to access the serial port, even if the user is part of 'dialout' group? Remove package brltty with: 'sudo apt remove brltty'. This is a ubuntu problem.
{% endhint %}

## Connect Radio

![](<../../.gitbook/assets/image (5).png>)

* install a jumper between pin 4 and 6
* Connect the modem to the computer&#x20;
  * using the provided USB-Serial adapter
  * OR: using the pin description above for your USB-Serial adapter

![Modem with the provided usb adapter connected](<../../.gitbook/assets/image (6).png>)

## Configure

* Connect to the modem using a serial console on your computer
  * Baudrate: 57600

```
sudo minicom -D /dev/ttyUSB0 -b 57600
```

* Get into configuration mode
  * During startup press "+++"
  * "OK" should appear to be able to change the configuration
  * More info can be found in [https://www.hs-augsburg.de/homes/beckmanf/dokuwiki/doku.php?id=searchwing-rf-antenne](https://www.hs-augsburg.de/homes/beckmanf/dokuwiki/doku.php?id=searchwing-rf-antenne)
* Check for firmware Version 1.91 (28-05-2022) with command "ATI". Firmware files and tools are found here: [http://files.rfdesign.com.au/](http://files.rfdesign.com.au/)
* Change parameters

| Command                  | Hint                                              |
| ------------------------ | ------------------------------------------------- |
| <p></p><p>AT&#x26;F </p> | factory reset parameters                          |
| <p></p><p>ATS4=20</p>    | <p></p><p>(PLANE) set txpower to 20dbm/100mW </p> |
| ATS4=30                  | (GROUNDSTATION) set txpower to 30dbm/1000mW       |
| ATS6=1                   | set data protocol type to MAVLINK                 |
| <p></p><p>AT&#x26;W</p>  | <p></p><p>write current changes to EEPROM</p>     |
| ATZ                      | reboot                                            |
| ATI5                     | check if changed values are set                   |

Should look like this:

```
Welcome to minicom 2.7.1

OPTIONS: I18n 
Compiled on Aug 13 2017, 15:25:34.
Port /dev/ttyUSB0, 17:21:12

Press CTRL-A Z for help on special keys

OK
AT&F
OK
ATS4=20
OK
ATS6=1
OK
AT&W
OK
ATZOK
ATI5
S0:FORMAT=26
S1:SERIAL_SPEED=57
S2:AIR_SPEED=64
S3:NETID=25
S4:TXPOWER=20
S5:ECC=0
S6:MAVLINK=1
S7:OPPRESEND=0
S8:MIN_FREQ=868000
S9:MAX_FREQ=869000
S10:NUM_CHANNELS=10
S11:DUTY_CYCLE=100
S12:LBT_RSSI=0
S13:MANCHESTER=0
S14:RTSCTS=0
S15:MAX_WINDOW=131
S16:ENCRYPTION_LEVEL=0
```

## Firmware update on Linux

Download the firmware:

```
wget https://files.rfdesign.com.au/Files/firmware/RFDSiK%20V1.91%20rfd900p.ihx                                                             
```

Download the \`uploader.py\` script from the open source SiK firmware:

```
wget https://raw.githubusercontent.com/ArduPilot/SiK/master/Firmware/tools/uploader.py
```

Install pymavlink and pyserial:

```
pip install pymavlink pyserial
```

Flash/upload the firmware to a connected device:

```
python uploader.py --port /dev/ttyUSB0 RFDSiK\ V1.91\ rfd900p.ihx
```

