---
description: check current version in text file on SD card
---

# Update RC firmware (OpenTX) and telemetry script (yaapu)

1. Start RC controller in debug mode by holding both horizontal trimmer buttons pressed towards the center while switching it on.
2. Connect RC controller via USB.
3. Find newest version for model X9DP and download SD content ([https://www.open-tx.org/downloads](https://www.open-tx.org/downloads))
4. Extract the ZIP file to the root/top level folder of the microSD Card.
5. Add telemtry scripts (copy content of [https://github.com/yaapu/FrskyTelemetryScript/tree/master/TARANIS/SD](https://github.com/yaapu/FrskyTelemetryScript/tree/master/TARANIS/SD) to SD card)
6. Copy new firmware file 'opentx-x9d+-eu-lua-de-2.x.yz-otx' (where to download? did it with the Companion software, see e.g. here [https://www.open-tx.org/2022/04/22/opentx-2.3.15](https://www.open-tx.org/2022/04/22/opentx-2.3.15)) to the FIRMWARE directory on the SD card.
7. Disconnect the USB.
8. Press ENT in order to write the new firmware.
9. Restart.
10. With a model set up ([https://app.gitbook.com/o/-MWAbYktIOVCqPHoTxRn/s/-MYsj38KDFg4fXvhUh7B/\~/changes/5sWj9SoPYT4tKrfUbrWj/preparing-components/software-setup/create-model-on-rc](create-model-on-rc.md)), press MENU and navigate to the model, press PAGE 13 times until you see 'Telemetry pages', navigate down to 'Page: 1', press ENT and PLUS (3 times) til 'Script', ENT, MINUS, ENT, select 'yaapu9' and ENT. Telemetry should be set up now.
