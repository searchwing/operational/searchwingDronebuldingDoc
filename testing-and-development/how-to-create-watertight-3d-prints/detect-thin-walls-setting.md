# Detect thin walls setting

We discovered strange tiny holes on the initial prints. We found these holes also in the slicer preview:

![Holes inside the wall of the box](../../.gitbook/assets/prusaslicer-setting-detect-thin-walls-deactivated.png)



![](../../.gitbook/assets/prusaslicer-setting-detect-thin-walls-deactivated-test-print.jpg)



![](../../.gitbook/assets/prusaslicer-setting-detect-thin-walls-deactivated-test-print-internal.jpg)

## Change the setting&#x20;

![](../../.gitbook/assets/prusaslicer-setting-detect-thin-walls.png)

### Result in the slicer

![](../../.gitbook/assets/prusaslicer-setting-detect-thin-walls-activated.png)

![](../../.gitbook/assets/prusaslicer-setting-detect-thin-walls-activated-test-print-internal.jpg)

Also the outer walls are much nicer now:

!["Detect thin walls" - deactivated](../../.gitbook/assets/prusaslicer-setting-detect-thin-walls-deactivated-test-print-outer.jpg)

!["Detect thin walls" - activated](../../.gitbook/assets/prusaslicer-setting-detect-thin-walls-activated-test-print-external.jpg)

