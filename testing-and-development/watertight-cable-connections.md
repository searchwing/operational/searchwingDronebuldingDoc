# Watertight cable connections

**Final results:** **Test with pressurized air showed that the connections with shrinking tube/hot glue are not waterproof on silicon and pvc cables.**

## Silicon vs. PVC Cables

Shrinking the cables watertight requires a good adhesion between the cable and the shrink tube / hot glue. Tests showed that silikon cables are not as good as normal PVC cables

![Muldental silicon cables vs. a normal cable with PVC](../.gitbook/assets/cablesiliconpvc-1.jpg)

![Shrinking works good on both types](../.gitbook/assets/cablesiliconpvc-2.jpg)

![Mechanical force dis-attached the silicon cables](../.gitbook/assets/cablesiliconpvc-3.jpg)

