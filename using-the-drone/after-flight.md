---
description: >-
  This section describes the operation of the GUI and the instructions to
  download images and logs from the Pi.
---

# After-Flight

## 1. Download Images

After the plane is landed, you should immediatly start downloading the images to the groundstation laptop through the image download station.

1. Start by pressing "Download Drone images" on Groundstation Laptop
2. Enter dronename - here "perlman"
3. Leave cam prefix empty
4. Two windows should appear - each for one camera / pi in the plane
5. Approximatly 1 image download should take 1-2 seconds.

![](<../.gitbook/assets/image (8).png>)

## 2. Image analysis

While image download is running, start analysis of the images while they get downloaded.

* There are different algorithms available to analyse the images&#x20;
  * Classify Images: Shows whether a image includes a boat but not where it is
    * EfficientNet
  * Detect Boats: Shows where in the image a boat is located
    * FRCNN
    * RetinaNet

{% hint style="info" %}
Be aware that only one analysis run / algorithm can run at a time! You NEED to close it by pressing STRG+C in the window where the processing takes / took place! DONT close it by just closing the window!
{% endhint %}

### Start analysis

1. Start analysis by clicking the corresponding desktop icon
2. Choose the currently downloaded images of the plane according to planename and timestamp from _/home/searchwing/flightdata/_
3. Press "OK"
4. All current and live added images to the folder will now be processed
5. Current progress and left over time can be observed

![](<../.gitbook/assets/image (16).png>)

![](<../.gitbook/assets/image (12).png>)

### Suggestion on how to process on new flight data (Best practice!)

1. Start "Image Classification" as it is the fastest algorithm
2. Wait until Download and classification is finished and close the window by pressing STRG+C
3. Start "Detect Boats (Retina)" as its more accurate than classification but takes longer

### Troubleshooting

#### Problems to start a analysis run

Sometimes it can happen that you cant start a new analysis run. This can be caused by:

* still running analysis in the background
* analysis window wasnt closed by using STRG+C on the window

You can fix this by closeing / killing all running docker container / analysis runs by typing the following command in a terminal:

```bash
docker kill $(docker ps -q)
```

## 3. Observe images

While the download and the analysis is running you should now observer the images and the analysis results by using the searchwing-gui.

1. Start GUI via desktop icon - The browser should open
2. Select / click the currently downloaded image data
3.  Read "Help" to get more usability info

    1. Use Arrowkeys to browse images
    2. Sort images using "DateTime" or "Algorithm"
    3. Press "Shift"-Key to temporarily hide Analysis Run Boxes
    4. Press "Enter"-Key to mark image as viewed and go to next image
    5. Press "Space"-Key to mark image as interesting / that there is REALLY a boat
    6. Current GPS Position is shown in the top bar
    7. Press "\<back" to choose a different flight dataset



![](<../.gitbook/assets/image (7).png>)

![](<../.gitbook/assets/image (9).png>)

![](<../.gitbook/assets/image (14).png>)

{% hint style="info" %}
If the GUI is already running and you try to open it up again a message will appear whether you want to open the GUI at another port. Dont press "Y" but instead open the browser and open the GUI via typing in "http://localhost:3000".
{% endhint %}

## 4. Recharge Battery

* [ ] Remove battery from the drone
* [ ] Plug in main lead and balance lead to the charger
* [ ] Select the right charging mode\*
* [ ] Press charge (process takes around one hour when the battery is fully discharged)

\*_The right charging mode depends on the type of the battery used (material, voltage - number of cells, S, and C load). This should be specified in the label provided with the battery. The setitngs for our battery are: LiPo 14.8 V (4S)._

__

_TODO include pictures with the right charger._

## (optional) Download pi log file

* [ ] Download the pi log file(s) via scp

```bash
$ mkdir -p /home/searchwing/flightdata/FLIGHTNAME/pi-logs
$ cd /home/searchwing/flightdata/FLIGHTNAME/pi-logs
$ scp searchwing@perlman-l:/data/logs/* .
```

## 5. Turn off systems

* [ ] UAV / drone
* [ ] Remote control
* [ ] QGC / tablet
* [ ] TODO

## 6. Documentation

### 6.1 Download logs from QCG (Optional)

_Include guide and screenshots to download logs from QCG GUI_

### 6.2. Checklist

* [ ] Get flight log / book
* [ ] Is goal reached?
* [ ] TODO

## 7.  \[Optional] Cleaning the drone

{% hint style="info" %}
If drone is not used anymore within the next hours cleaning is needed!
{% endhint %}

### Clean motor using sweetwater

* TODO
