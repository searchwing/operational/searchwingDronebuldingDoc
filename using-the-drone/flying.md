# Flying

## Checklist



* [ ] GPS lock (number of satellites >= 4), geofence
* [ ] Arm plane
* [ ] Test full throttle (beware of the propeller!)
* [ ] Disarm
* [ ] Clear area, biwarn bystanders

## Start the plane

* [ ] Launcher: Put on protection glove
* [ ] arm
* [ ] launcher takes plane and stands up against wind
* [ ] launcher gives ok signal
* [ ] switch to Mission Mode, running motor
* [ ] throw horizontally

## During Flight

* [ ] Pilot: maintain visual contact
* [ ] Observer: Monitor QGC
* [ ] Return to home: In the own GPS position and click "go-here" in QGC (this might need to be applied multiple times in a moving ship)

## Landing



* [ ] Clear area, warn bystanders
* [ ] switch to stabilized (manual) mode
* [ ] Visually define the landing area:
  * [ ] In water - Check that the waves are not very big
  * [ ] Check for other boats or bystanders
* [ ] Descend and slowly decrease the throttle
* [ ] When close to land turn throttle off (0%) and nose-up
* [ ] confirm landing
* [ ] disarm UAV
* [ ] collect plane
* [ ] remove prop
* [ ] check for damage
