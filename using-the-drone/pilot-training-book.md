---
description: building a 'how to fly'
---

# Pilot training book

## About the handbook

The purpose of this chapter is to group the most important information needed to safely operate the system.

**Attention**: So far, it's under construction and far from being complete!

NB: Please also check out the other pages of "Using the drone'.

## Basic Introduction of the UAV/UAS

This section will give you a short overview about the system.

### Components of the system (UAV + Ground Station)

This section describes a list of the main components of the system (airborne and ground sides).

#### Drone (Airborne side)

* X-UAV Mini Talon (Including controller, servos...)
* RC Controller Frsky Taranis X9D Plus
* Battery (LiPo 4S)
* Battery charger
* Rapsberry Pi w/ camera (aka. payload)

#### Ground station

* Power module for the battery
* Telemetry Groundstation Box (RFD868)
* Laptop (Ground Station) - includes the following software:
  * [QGroundControl QGC](https://docs.qgroundcontrol.com/): Configuration of the vehicle, mission planning and monitoring.
  * Searchwing GUI
  * [OpenTX](https://doc.open-tx.org/): Software to set up the RC configuration

## Basic unit conversions at land and sea

The operation of the system is in the context of different units systems, at land or at sea. Hence, these conversions might be useful for the user:

* 1 km/h = 0.54 knots
* 1 NM (Nautic Mile) = 1.852 km
* 1 ft = 0.305 m

## Basic communication

(WIP)

* How to use the walkie-talkie
  * Always push to talk
  * Finish the sentence with a keyword, so that the other user know that your sentence has finished.
* Use clear and precise communication
* It is better ot overcommunicate than undercommunicate

### Communication with the recovery crew and/or bridge

Make sure that all participants know what will happen and what happens. Make sure all participants are reachable via walkie-talkie. Do a radio check!

#### Before takeoff

Inform bridge about planed takeoff in e.g. 10 minutes. Request change of course if necessary, e.g to start the UAV with head wind.

#### Takeoff

Request permission for takeoff. Inform bridge about successful takeoff

#### In flight

Update bridge about flight if necessary.

#### Before landing

Request permission to land in e.g. 10 minutes from bridge. Inform bridge and recovery crew prior to landing. Inform them about planed position of landing.

#### After landing

Inform bridge and recovery crew about position of the plane. If the UAV is disarmed inform recovery crew that the UAV is safe for recovery.

Await feedback from recovery crew about successful recovery.

## Documentation

* How to document flights etc.

## Legal restrictions

(WIP)&#x20;

Legal situation on land, e.g. at test flights and legal situation at sea, in e.g. international waters/airspaces.

(based on [https://www.easa.europa.eu/sites/default/files/dfu/AMC%20%26%20GM%20to%20Part-UAS%20%E2%80%94%20Issue%201.pdf](https://www.easa.europa.eu/sites/default/files/dfu/AMC%20%26%20GM%20to%20Part-UAS%20%E2%80%94%20Issue%201.pdf))

Question: what applies at sea?

* basic pilot license (Kenntnisnachweis)
  * https://lba-openuav.de/einstieg/
* operator of the UAV
  * operator id
* insurance

## Groundstation

The groundstation is composed by a laptop (and/or tablet) and a telemetry station that is used to communicate with the drone via radio. The ground station is mainly composed of the flying control software (QGC) and the image recognization software. The first, is used to setup everything related with the UAV, prepare the mission and monitor the flight, and it is the main interface of communication with the drone.

### Short guide to QGroundControl

This section provides instructions to perform basic operations using QGroundControl. A more detailed guide regarding how to use the tool, can be found in [untitled.md](untitled.md "mention").

#### Set up the vehicle

The setup view is used to configure the vehicle, including sensor calibration, setting up flight modes or checking the radio connection. Set up should be performed prior flying and the parameters are saved for latter use, so what you should see is the following screen with only green lights:&#x20;

![](https://i.imgur.com/y5EoMAT.png)

In general, most of the set up should be performed only once, and only from time to time the following maintainance tasks should be done:

* Accelerometer and gyroscope calibration (on land always) every 14 to 30 days.
* Magnetometer calibration, every 7 to 14 days
* Battery calibration, every 30 days

***

> If you want to modify some configuration, such as sensor calibration, make sure that you are in a space with no motion and far from iron structures (i.e. not on a ship), as this will affect the accuracy of the calibration!

#### Flight preparation

**Flight plan**

for using QGroundControl (QGC), see [untitled.md](untitled.md "mention").

**Preparing a mission - Scenarios**

1. **Reported case**. Fly to reported position and fly pattern: lawn mower or SAR (e.g. expanding square search, to be implemented).
2. **Finish line**. Fly some lines or always the same line (?) and back. This might be some line to be crossed by a target boat either by engine or when drifting (wind/current).
3. **Accompany the ship**. Example assuming constant speed (no wind): Ship going south with 5 kn, drone with 25kn, thus excess speed of 20 kn. Fly 5/25 of the time parallel to ship, (20/2)/25=10/25 of the time east and other 10/25 of the time back (west). Free choice of order for three legs? In order to cover the area without overlap\*, one might fly 5 min (4000m) east, 5min (4000m) west, and 2.5 min south (2000m), repeatedly.

\*At an altitude of 550m, the sector recorded is about 2000m wide, which is crossed within \~2.5 min at 45 km/h.

When planning a mission and battery consumption, compare the final distance to be traveled displayed in QGC with the drone's capability (\~40 km/h) and mind the wind.

**Pre-arm safety checks**

QGC includes by default several checks in order to arm the plane. These can be deacivated, but it is extremely not recommended to do so! Best practice is to enable all of them.

#### Flight Monitoring

The Fly View is used to command and monitor the vehicle when flying. &#x20;

![](https://i.imgur.com/IYTXTCi.jpg)

You can use it to:

```
1. Run an automated pre-flight checklist.
2. Control missions: start, continue, pause, and resume.
3. Guide the vehicle to arm/disarm/emergency stop, takeoff/land, change altitude, go to or orbit a particular location, and return/RTL.
4. Switch between a map view and a video view (if available)
5. Display video, mission, telemetry, and other information for the current vehicle, and also switch between connected vehicles.
```

#### Log download

The Log Download screen **(Analyze > Log Download)** is used to list (Refresh), Download and Erase All log files from the connected vehicle.

![](https://i.imgur.com/c9neaCi.png)

### Searchwing GUI handbook

See after [flight](after-flight.md) section.

## Operating the drone

This section includes important remarks about using the drone.

### Flight checklist

It is crucial to follow all steps mentioned in this checklist. Best practice is to print it out and check all steps before every flight.

(Link to the checklist)

### People involved

1. **Observer**: The person that has no direct control of the plane but is in charge of monitoring the flight. Duties:
   * Monitoring QGC
   * Defining the flight path
   * Asist with checklist
   * Document post-flight
2. **Pilot**. Person in charge of operating the plane. Duties:
   * Arm/Disarm plane
   * Hold the RC at every time.
   * etc...
3. **By standers**

### Using the RC

(WIP)

Question: Which buttons to press?

### Switching on

(WIP)

TODO maybe link the original documentation of the RC controler and OpenTX

### Configuration

(WIP)

TODO choose the plane/model

TODO start telemetry script

## Basic flying skills

Question: What maneuvers are to be mastered with the aircraft?

### Takeoff

Basic instructions:

* do not push it but rather lift it... etc
* insert nice movie of a takeoff ...

### Landing

Basic instructions (WIP)

1. Slowly descend to a lower altitute (\~ 100 to 50 m)
2. When the drone is inside the line of sight, switch to stabilized mode and initiate the descend.
3. Beware that your speed is not too high (**insert num here**) and slowly reduce power.
4. When the airframe is close to the ground, reduce power to 0 and elevate nose up to touch the surface.

### Go-Around

Abort from a bad landing approach.

### TODO

* fly an eight
* fly a circle
* fly other pattern
* fly by instrument
  * return to home by instruments

### After flight

## Advanced flying skills

Question: optional training

## FAQs

## Safety

* approach the plane from a safe direction
* do not arm before checklist complete
* wear safety cloth
* Not arming before being 100% sure ready to flight
* Beware of others

## Emergency

**(WIP)**

This section compiles a series of emergency situations and what to do in each case.

1. What to do if another airplane approaches (TBD)
2. Communication in emergencies (TBD) first aid
3. Emergency plans (TBD)
4. What to do in case of a fly away
   * TBD
   * who to contact, how to contact them
5. What if QGroundControl Crashes?
   * In this case, and if the visual conditions allow it, the drone operator should inmediately switch to stabilized mode and return back manually.
6. What if the RC controller does no work?
   * If QGC is operative, use the return to home function to send the drone back to your position.

## Troubleshooting

1. Broken parts
2. Servo does not work
3. QGroundControl configuration has disappeared
