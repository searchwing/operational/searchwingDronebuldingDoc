# Pre-Flight

## Plan your flight

* [ ] Determine the goal of the flight
* [ ] Plan flight sequence (take-off, flight and mission, landing; which modes)
* [ ] Plan and validate mission
  * [ ] If required: load offline maps into QGC Plan mission in QGC (consider duration and range!)
  * [ ] Set QGeoFence sensibly
  * [ ] Transfer mission to aircraft (will only be verified during upload)
  * [ ] Save verified mission
* [ ] Briefing
  * [ ] procedure/explain mission
  * [ ] abortion criteria
  * [ ] determine observer, protocol
* [ ] **Editing positions** (coordinates) in QGroundControl
  * [ ] Enter coordinates waypoint/pattern by clicking ☰ > Edit position... (right hand side) or drag\&drop
  * [ ] Set Altitude: 550 m
  * [ ] For a search pattern, click Pattern > Survey > Basic/Circular and edit values:
    * [ ] Trigger Distance (for a camera, not used)
    * [ ] Spacing: 1800 m (theoretically 2068 m w/o overlap)
    * [ ] You may want to Rotate Entry Point for a quicker start.

![](<../.gitbook/assets/image (4).png>)

* [ ] (optional) Sun position dependent flying to reduce image reflections
  * [ ] Cameras of the plane are to the left and right of the plane - thus the sun should be always infront or behind the drone to reduce reflections
  * [ ] Choose the angle of the pattern flight the same as the current sun azimuth

![Reduce image reflections by choosing a pattern angle according to sun position - e.g. its 2:30pm so we choose 13° as pattern angle](<../.gitbook/assets/image (19) (1) (1) (1).png>)

## Upload your flight

## Checklist

* [ ] Check communication channels with bridge and recovery crew
* [ ] Battery charged (16.8 V)
* [ ] Tighten the screws in the wings
* [ ] Check for visual defects
  * [ ] Klappen Flügel (noch fest?)
  * [ ] Klappen V-Tails
  * [ ] Servohörner fest?
  * [ ] Kamera
  * [ ] Membran
  * [ ] Deckel auf Ladebuchse?
  * [ ] Antenne noch fest?
  * [ ] Turm noch fest?
  * [ ] Akku-Tüte zu?
* [ ] ‌Check M plugs (connecting V tail servos with box)
* [ ] ‌check barycentre ("schwerpunkt")
* [ ] ‌note weight
* [ ] ‌turn QGC, RC, UAV on
* [ ] Payload
  * [ ] Clean camera window with a tissue
  * [ ] Check preflight-selfcheck of payloads in QGC
    * [ ] All services are running (camera + dht22 + mavlink-router)
    * [ ] SD card got enough space for a 1h flight -  delete old images if necessary
    * [ ] Camera can take and save photos to SD Card
    * [ ] DHT22 humidity and temperature acceptable
    * [ ] PI CPU temperature acceptable
  * [ ] Check camera image quality in browser via wifi connection`http://droneName/0_latest.jpg`
* [ ] ‌AHRS (Attitude Heading Reference System) check&#x20;
* [ ] ‌Barometer, GPS check
* [ ] compass Check
* [ ] ‌Servo check (RC and Autopilot). (Plane in a steady position and RC in stab. mode):
  * [ ] When pitching up, elevators (tail) go down
  * [ ] When rolling right (left), right (left) aleiron goes up
* [ ] Safety parameters?
* [ ] ‌Save parameter set
* [ ] Propeller 10x7" r installed (font in direction of flight) and locked
* [ ] test Motor (-> vibration is bad is inlay in propeller?)

